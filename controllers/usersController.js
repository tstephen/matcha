const UserModel = require('../models/userModel');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const nodemailer = require('nodemailer');


const myTrim = function (x) {
	return x.replace(/^\s+|\s+$/gm, '');
}

const transporter = nodemailer.createTransport({
	service: 'gmail',
	auth: {
		user: 'matcha.tstephen@gmail.com',
		pass: 'n9QYE23uUy7K'
	}
});

let mailOptions = {
	from: 'no-reply@matcha.com',
	to: 'myfriend@yahoo.com',
	subject: 'Matcha Verification Token'
	/* text: 'That was easy!' */
};

function randomsort(a, b) {
	return Math.random() > .5 ? -1 : 1;
}

const generateToken = function () {
	let str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	var randomStr = str.split('').sort(randomsort).join('').substring(0, 5);
	return randomStr;
}

const uniqueUser = function (newUser, callback) {
	// Check if username is unique   
	UserModel.isUnique("username", newUser.username, (err, unique) => {
		if (err) {
			return callback(err, null);
		}
		if (!unique) {
			return callback(err, { success: false, msg: newUser.username + " is already taken." });
		} else {
			// Check if email is unique
			UserModel.isUnique("email", newUser.email, (err, unique) => {
				if (err) {
					return callback(err, null);
				}
				if (!unique) {
					return callback(err, { success: false, msg: newUser.email + " is already taken." });
				} else {
					return callback(null, { success: true });
				}
			});
		}
	});
}

module.exports.registerUser = function (user, callback) {
	// Make sure all fields are correct
	let msg;
	let isValid;
	let token = generateToken();

	const newUser = {
		fname: myTrim(user.fname),
		lname: myTrim(user.lname),
		email: myTrim(user.email),
		username: myTrim(user.username),
		password: user.password,
		interests: [],
		sexual_preference: '',
		gender: '',
		biography: '',
		birthDate: { month: 0, day: 0, year: 1900 },
		images: [],
		location: {
			type: "Point",
			coordinates: [user.longitude, user.latitude]
		},
		token: token,
		active: false
	}

	if (!newUser.fname || !newUser.lname || !newUser.email || !newUser.username ||
		!newUser.password) {
		msg = { success: false, msg: 'You must complete all the required fields.' };
		return callback(null, msg);
	}

	if (newUser.fname == '' || newUser.lname == '' ||
		newUser.username == '' || newUser.email == '' || newUser.password == '') {
		msg = { success: false, msg: 'You must complete all the required fields.' };
		return callback(null, msg);
	}

	isValid = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/.test(newUser.password);
	if (!isValid) {
		msg = { success: false, msg: 'Your password does not meet the minimum requirements.' };
		return callback(null, msg);
	}

	isValid = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(newUser.email);
	if (!isValid) {
		msg = { success: false, msg: 'Your email address is not valid.' };
		return callback(null, msg);
	}

	isValid = newUser.username.length >= 8 ? true : false;
	if (!isValid) {
		msg = { success: false, msg: "Your username must be at least 8 characters" };
		return callback(null, msg);
	}

	// If all tests passed register the user
	uniqueUser(newUser, (err, msg) => {
		if (err) {
			return callback(err, null);
		}

		if (msg.success === false) {
			return callback(null, msg);
		} else {
			//send email

			mailOptions['html'] = "<h1>Welcome To Matcha</h1><p>Click here and enter your token <a href='http://localhost:4200/verifyEmail'>Verify My Email</a></p>" + "<h4>" + token + "</h4>";
			mailOptions['to'] = newUser.email;

			transporter.sendMail(mailOptions, function (error, info) {
				if (error) {
					return callback(err, null);
				} else {
					console.log('Email sent: ' + info.response);
					// Add user to database
					UserModel.addUser(newUser, (err, user) => {
						if (err) {
							return callback(err, null);
						} else {
							return callback(null, { success: true, msg: "You have been registered successfully" });
						}
					});
				}
			});
		}
	});
}

module.exports.loginUser = function (username, password, callback) {
	UserModel.getUserByUsername(username, (err, user) => {
		if (err) {
			callback(err, null);
		}

		if (!user) {
			return callback(null, { success: false, msg: "User not found." });
		} else {
			UserModel.comparePassword(password, user.password, (err, isMatch) => {
				if (err) {
					return callback(err, null);
				} else {
					if (isMatch) {

						if (!user.active) {
							return callback(null, {
								success: false,
								msg: "You need to verify your account first. Please check your email."
							});
						}

						let signUser = {
							_id: user._id,
							fname: user.fname,
							lname: user.lname,
							email: user.email,
							username: user.username,
							active: user.active
						};

						const token = jwt.sign(signUser, config.secret, {
							expiresIn: 604800 // 1 week
						});

						return callback(null, {
							success: true,
							token: 'JWT ' + token,
							user: {
								id: user._id,
								name: user.name,
								username: user.username,
								email: user.email,
								active: user.active
							}
						});
					} else {
						return callback(null, {
							success: false,
							msg: "Wrong Password."
						});
					}
				}
			});
		}
	});
}

const getUpdateFields = function (newData, userId, callback) {
	UserModel.getUserById(userId, (err, user) => {
		if (err) {
			return callback(err, null);
		}

		// Verify that there are some fields to update and remove if not new
		let toUpdate = {};
		if (typeof newData.fname !== 'undefined') {
			newData.fname = myTrim(newData.fname);
			if (newData.fname !== user.fname && newData.fname != '')
				toUpdate['fname'] = newData.fname;
		}
		if (typeof newData.lname !== 'undefined') {
			newData.lname = myTrim(newData.lname);
			if (newData.lname !== user.lname && newData.lname != '')
				toUpdate['lname'] = newData.lname;
		}
		if (typeof newData.username !== 'undefined') {
			newData.username = myTrim(newData.username);
			let isValid = newData.username.length >= 8 ? true : false;
			if (!isValid) {
				msg = { success: false, msg: "Your username must be at least 8 characters" };
				return callback(null, msg);
			}

			if (newData.username !== user.username && newData.username != '')
				toUpdate['username'] = newData.username;
		}
		if (typeof newData.email !== 'undefined') {
			newData.email = myTrim(newData.email);
			if (newData.email !== user.email && newData.email != '')
				toUpdate['email'] = newData.email;

			let isValid = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(newData.email);
			if (!isValid) {
				msg = { success: false, msg: 'Your email address is not valid.' };
				return callback(null, msg);
			}
		}
		if (typeof newData.password !== 'undefined') {
			let isValid = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/.test(newData.password);
			if (!isValid) {
				msg = { success: false, msg: 'Your password does not meet the minimum requirements.' };
				return callback(null, msg);
			}
			toUpdate['password'] = newData.password;
		}

		if (typeof toUpdate.username !== 'undefined' || typeof toUpdate.email !== 'undefined') {
			// verify unique
			if (typeof toUpdate.username !== 'undefined') {
				if (typeof toUpdate.email !== 'undefined') {
					uniqueUser(toUpdate, (err, msg) => {
						if (err) {
							return callback(err, null);
						}

						if (msg.success === true) {
							return callback(null, toUpdate);
						} else {
							return callback(null, msg);
						}
					});
				} else {
					UserModel.isUnique("username", toUpdate.username, (err, unique) => {
						if (err) {
							return callback(err, null);
						} else {
							if (unique) {
								return callback(null, toUpdate);
							} else {
								return callback(null, { success: false, msg: toUpdate.username + " is already taken." });
							}
						}
					});
				}
			} else {
				UserModel.isUnique("email", toUpdate.email, (err, unique) => {
					if (err) {
						return callback(err, null);
					} else {
						if (unique) {
							return callback(null, toUpdate);
						} else {
							return callback(null, { success: false, msg: toUpdate.email + " is already taken." });
						}
					}
				});
			}
		} else {
			return callback(null, toUpdate);
		}
	});
}

module.exports.updateProfile = function (newData, userId, callback) {
	getUpdateFields(newData, userId, (err, toUpdate) => {
		if (err) {
			return callback(err, null);
		} else {
			if (typeof toUpdate.success !== 'undefined') {
				return callback(null, toUpdate);
			} else {

				if (!Object.keys(toUpdate).length) {
					return callback(null, { success: false, msg: "No information to be updated detected." });
				}

				if (typeof toUpdate.email !== 'undefined') {
					toUpdate['active'] = false;
					let token = generateToken();
					toUpdate['token'] = token;

					mailOptions['html'] = "<h1>Matcha - Updated Email Address</h1><p>Click here and enter your token <a href='http://localhost:4200/verifyEmail'>Verify My Email</a></p>" + "<h4>" + token + "</h4>";
					mailOptions['to'] = toUpdate.email;

					transporter.sendMail(mailOptions, (error, info) => {
						if (error) {
							return callback(error, null);
						}
					});
				}

				UserModel.updateUser(toUpdate, userId, (err, doc) => {
					if (err) {
						return callback(err, null);
					} else {
						doc["success"] = true;
						return callback(null, doc);
					}
				});
			}
		}
	});
}

module.exports.getUserInterests = function (userId, callback) {
	UserModel.userInterests(userId, (err, interests) => {
		if (err) {
			return callback(err, null);
		} else {
			return callback(null, interests);
		}
	});
}

module.exports.getDefaultInterests = function () {
	const defaultInterests = [
		"Personal growth",
		"Health and exercise",
		"Food",
		"Family",
		"Dancing",
		"Politics",
		"Travel",
		"Theater",
		"Learning",
		"Photography",
		"Charitable",
		"Finding love",
		"Outdoor interests",
		"Art",
		"Friends",
		"Work / Career"
	];

	return defaultInterests;
}

module.exports.updateUserInterests = function (userId, interests, callback) {
	let newInterests = [];

	for (let elem in interests) {
		newInterests.push(elem);
	}

	UserModel.updateUserInterests(userId, newInterests, (err, msg) => {
		if (err) {
			return callback({ success: false, msg: "Something went wrong. Please try again later" });
		} else {
			return callback(msg);
		}
	});
}

module.exports.updateUserGender = function (userId, gender, callback) {
	if (gender === 'Male' || gender === "Female") {
		UserModel.updateGender(userId, gender, (err, msg) => {
			if (err) {
				return callback(err, null);
			} else {
				let msg = { success: true, msg: "Your gender has been successfully updated" }
				return callback(null, msg);
			}
		});
	} else {
		let msg = { success: false, msg: "Your gender must be one of Male or Female" };
		return callback(null, msg);
	}
}

module.exports.getUserGender = function (userId, callback) {
	UserModel.getUserGender(userId, (err, gender) => {
		if (err) {
			return callback(err, null);
		} else {
			if (!gender) {
				return callback(null, { success: false, msg: "Gender is not yet set." });
			} else {
				return callback(null, { success: true, gender: gender });
			}
		}
	});
}

module.exports.updateUserSexualPreference = function (userId, preference, callback) {
	if (preference === "Heterosexual" || preference === "Homosexual" || preference === "Bisexual") {
		UserModel.updateSexualPreference(userId, preference, (err, msg) => {
			if (err) {
				return callback(err, null);
			} else {
				let msg = { success: true, msg: "Your sexual preference has been successfully updated" }
				return callback(null, msg);
			}
		});
	} else {
		let msg = { success: false, msg: "Your sexual preference must be one of Heterosexual, Homosexual or Bisexual" };
		return callback(null, msg);
	}
}

module.exports.getUserSexualPreference = function (userId, callback) {
	UserModel.getUserSexualPreference(userId, (err, preference) => {
		if (err) {
			return callback(err, null);
		} else {
			if (!preference) {
				return callback(null, { success: false, msg: "Sexual preference is not yet set." });
			} else {
				return callback(null, { success: true, sexual_preference: preference });
			}
		}
	});
}

module.exports.getUserBio = function (userId, callback) {
	UserModel.getUserBio(userId, (err, msg) => {
		if (err) {
			return callback({ success: false, msg: "Something went wrong. Please try again later." });
		} else {
			return callback(msg);
		}
	});
}

module.exports.updateUserBio = function (userId, biographyText, callback) {
	UserModel.updateUserBio(userId, biographyText, (err, msg) => {
		if (err) {
			return callback({ success: false, msg: "Something went wrong. Please try again later." });
		} else {
			return callback(msg);
		}
	});
}

module.exports.updateUserDateOfBirth = function (userId, birthDateObject, callback) {

	if (!birthDateObject) {
		return callback({ success: false, msg: "Please format your request correctly." });
	}

	if (!birthDateObject.month || !birthDateObject.year || !birthDateObject.day) {
		return callback({ success: false, msg: "You must provide a year, month and day." });
	}

	let day = parseInt(birthDateObject.day);
	let month = parseInt(birthDateObject.month);
	let year = parseInt(birthDateObject.year);

	if (!day || !month || !year) {
		return callback({ success: false, msg: "Your date values must be integers." });
	}

	let todayDate = new Date();

	if ((day < 1 || day > 31) || (month < 1 || month > 12) || year < 1900) {
		return callback({ success: false, msg: "Your have provided an invalid date" });
	}

	if (year > todayDate.getFullYear() - 13) {
		return callback({ success: false, msg: "You must be at least 13 years old." });
	}

	UserModel.updateBirthDate(userId, birthDateObject, (err, msg) => {
		if (err) {
			return callback({ success: false, msg: "Something went wrong. Please try again later." });
		} else {
			return callback({ success: true, msg: "Your birthdate has been successfully updated." });
		}
	});
}

module.exports.getUserBirthDate = function (userId, callback) {
	UserModel.getUserBirthDate(userId, (err, msg) => {
		if (err) {
			return callback({ success: false, msg: "Something went wrong. Please try again later" });
		} else {
			return callback({ success: true, msg: "Your birthdate has been successfully retrieved.", birthdate: msg });
		}
	});
}

module.exports.uploadImage = function (userId, imageNumber, dataURL, callback) {
	if (!imageNumber || !dataURL) {
		return callback({ success: false, msg: "You need to provide both a imageNumber and a dataURL" });
	}

	UserModel.saveImage(userId, imageNumber, dataURL, (err, msg) => {
		if (err) {
			return callback({ success: false, msg: "Something went wrong. Try again later." });
		} else {
			return callback({ success: true, msg: "Your image has been successfully uploaded." });
		}
	});
}

module.exports.getImages = function (userId, callback) {
	UserModel.getImages(userId, (err, msg) => {
		if (err) {
			return callback({ success: false, msg: "Unable to fetch images. Please try again later." });
		} else {
			return callback({ success: true, msg: "Your images have been successfully retrieved.", images: msg });
		}
	});
}

module.exports.getLocation = function (userId, callback) {
	UserModel.getLocation(userId, (err, msg) => {
		if (err) {
			return callback({ success: false, msg: "Something went wrong getting your location. Please try again later." });
		} else {
			return callback({ success: true, msg: "Your location has been successfully retrieved.", location: msg });
		}
	});
}

module.exports.updateLocation = function (userId, lat, lng, callback) {

	let latitude = parseFloat(lat);
	let longitude = parseFloat(lng);

	if (!lat || !lng) {
		return callback({ success: false, msg: "Please provide both a latitude and longitude." });
	}

	if (!latitude || !longitude) {
		return callback({ success: false, msg: "Invalid values provided" });
	}

	if (latitude < -90 || latitude > 90) {
		return callback({ success: false, msg: "Invalid values provided" });
	}

	if (longitude < -180 || longitude > 180) {
		return callback({ success: false, msg: "Invalid values provided" });
	}

	let coords = [longitude, latitude];

	UserModel.updateLocation(userId, coords, (err, msg) => {
		if (err) {
			return callback({ success: false, msg: "Something went wrong. Please try again later." });
		}
		return callback({ success: true, msg: "Your location has been successfully updated." });
	})
}

module.exports.verifyEmail = function (username, token, callback) {
	if (!username || !token) {
		return callback({ success: false, msg: "You need to provide a username and a token" });
	}

	UserModel.verifyEmail(username, token, (err, msg) => {
		if (err) {
			return callback({ success: false, msg: "Something went wrong. Please try again later" });
		} else {
			return callback(msg);
		}
	});
}

module.exports.forgotPassword = function(username, callback) {
	if (!username) {
		return callback({success: false, msg: "User not found."});
	}

	let token = generateToken();

	UserModel.generatePasswordToken(username, token, (err, msg) => {
		if (err) {
			return callback({success: false, msg: "Something went wrong. Try again later."});
		} else {
			
			mailOptions['html'] = "<h1>Matcha - Forgot Password</h1><p>Enter your token " + token + "</p>";
			mailOptions['to'] = msg.email;
			transporter.sendMail(mailOptions, function (error, info) {
				if (error) {
					return callback({success: false, msg: "Something went wrong. Try again later."});
				}

				return callback({success: msg.success, msg: msg.msg});
			});
		}
	});
}

module.exports.newPassword = function(username, token, password, callback) {
	if (!username || !password || !token) {
		return callback({success: false, msg: "You need to provide a username, password and token."});
	}

	let isValid = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/.test(password);
	if (!isValid) {
		msg = { success: false, msg: 'Your password does not meet the minimum requirements.' };
		return callback(msg);
	}

	UserModel.updatePassword(username, token, password, (err, msg) => {
		if (err) {
			return callback({success: false, msg: "Something went wrong. Please try again later."});
		} else {
			return callback(msg);
		}
	});
}

module.exports.findByLocation = function(userId, distance, callback) {
	distance = parseInt(distance);
	
	if (distance <= 0) {
		return callback({success: false, msg: "Distance must be a positive integer."});
	}

	UserModel.findByLocation(userId, distance, (err, msg) => {
		if (err) {
			return callback({success: false, msg: "Failed to find matches"});
		}
		return callback({success: true, msg: "Your suggestions have been retrieved.", suggestions: msg});
	});
}