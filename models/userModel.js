//const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const MongoConnection = require('../config/connection');
const ObjectiD = require('mongodb').ObjectID;

module.exports.addUser = function (newUser, callback) {
	MongoConnection.db.collection('users').createIndex({location: "2dsphere"});
	bcrypt.genSalt(10, (err, salt) => {
		if (err) {
			return callback(err, null);
		}
		bcrypt.hash(newUser.password, salt, (err, hash) => {
			if (err) {
				callback(err, null);
			}
			newUser.password = hash;
			MongoConnection.db.collection('users').insertOne(newUser, callback);
		});
	});
}

module.exports.getUserByUsername = function (username, callback) {
	MongoConnection.db.collection('users').findOne({ "username": username }, (err, user) => {
		if (err) {
			return callback(err, null);
		}
		return callback(null, user);
	});
}

module.exports.getUserById = function (userId, callback) {
	userId = new ObjectiD(userId);
	MongoConnection.db.collection('users').findOne({ _id: userId }, callback);
}

module.exports.comparePassword = function (candidatePassword, hash, callback) {
	bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
		if (err) {
			return callback(err, null);
		}
		return callback(null, isMatch);
	});
}

module.exports.isUnique = function (field, value, callback) {
	let toFind = {};
	toFind[field] = value;

	MongoConnection.db.collection('users').findOne(toFind, (err, user) => {
		if (err) {
			return callback(err, null);
		} else if (!user) {
			return callback(null, true);
		} else {
			return callback(null, false);
		}
	});
}

module.exports.updateUser = async function (toUpdate, userId, callback) {

	if (typeof toUpdate.password !== 'undefined') {
		bcrypt.genSalt(10, (err, salt) => {
			if (err) {
				return callback(err, null);
			}
			bcrypt.hash(toUpdate.password, salt, (err, hash) => {
				if (err) {
					callback(err, null);
				}
				toUpdate.password = hash;
				MongoConnection.db.collection('users').findOneAndUpdate({ _id: userId }, { $set: toUpdate }, (err, doc) => {
					if (err) {
						return callback(err, null);
					} else {
						return callback(null, doc.value);
					}
				});
			});
		});
	} else {
		MongoConnection.db.collection('users').findOneAndUpdate({ _id: userId }, { $set: toUpdate }, (err, doc) => {
			if (err) {
				console.log(err);
				return callback(err, null);
			} else {
				return callback(null, doc.value);
			}
		});
	}

}

module.exports.userInterests = function (userId, callback) {
	MongoConnection.db.collection('users').findOne({ _id: userId }, (err, user) => {
		if (err) {
			return callback(err, null);
		} else {
			return callback(null, user.interests);
		}
	});
}

module.exports.updateUserInterests = function (userId, interests, callback) {
	MongoConnection.db.collection('users').replaceOne(
		{ _id: userId },
		{
			$set: {
				interests: interests
			}
		},
		(err, msg) => {
			if (err) {
				return callback(err, null);
			} else {
				return callback(null, { success: true, msg: 'Your interests have been successfully updated' })
			}
		}
	);
}

module.exports.updateGender = function (userId, gender, callback) {
	MongoConnection.db.collection('users').updateOne(
		{ _id: userId },
		{
			$set: {
				gender: gender
			}
		},
		(err, msg) => {
			if (err) {
				return callback(err, null);
			} else {
				return callback(null, msg);
			}
		}
	);
}

module.exports.getUserGender = function (userId, callback) {
	MongoConnection.db.collection('users').findOne(
		{ _id: userId },
		(err, profile) => {
			if (err) {
				return callback(err, null);
			} else {
				return callback(null, profile['gender']);
			}
		}
	);
}

module.exports.updateSexualPreference = function (userId, preference, callback) {
	MongoConnection.db.collection('users').updateOne(
		{ _id: userId },
		{
			$set: {
				sexual_preference: preference
			}
		},
		(err, msg) => {
			if (err) {
				return callback(err, null);
			} else {
				return callback(null, msg);
			}
		}
	);
}

module.exports.getUserSexualPreference = function (userId, callback) {
	MongoConnection.db.collection('users').findOne(
		{ _id: userId },
		(err, profile) => {
			if (err) {
				return callback(err, null);
			} else {
				return callback(null, profile['sexual_preference']);
			}
		}
	);
}

module.exports.getUserBio = function (userId, callback) {
	MongoConnection.db.collection('users').findOne(
		{ _id: userId },
		(err, profile) => {
			if (err) {
				return callback(err, null);
			} else {
				return callback(null, { success: true, bio: profile['biography'], msg: 'Your biography was successfully retrieved.' });
			}
		}
	);
}

module.exports.updateUserBio = function (userId, biographyText, callback) {
	MongoConnection.db.collection('users').updateOne(
		{ _id: userId },
		{
			$set: {
				biography: biographyText
			}
		},
		(err, data) => {
			if (err) {
				return callback(err, null);
			} else {
				return callback(null, { success: true, msg: "Your biography has been successfully updated" });
			}
		}
	);
}

module.exports.updateBirthDate = function (userId, birthDateObject, callback) {
	MongoConnection.db.collection('users').updateOne(
		{ _id: userId },
		{
			$set: {
				birthDate: {
					month: birthDateObject.month,
					day: birthDateObject.day,
					year: birthDateObject.year
				}
			}
		},
		(err, msg) => {
			if (err) {
				return callback(err, null);
			} else {
				return callback(null, msg);
			}
		}
	);
}

module.exports.getUserBirthDate = function (userId, callback) {
	MongoConnection.db.collection('users').findOne(
		{ _id: userId },
		(err, profile) => {
			if (err) {
				return callback(err, null);
			} else {
				return callback(null, profile.birthDate);
			}
		}
	);
}

module.exports.saveImage = function (userId, imageNumber, dataUrl, callback) {

	let imageObject = {};
	imageObject[imageNumber] = dataUrl;

	MongoConnection.db.collection('users').findOne(
		{ _id: userId },
		(err, user) => {
			if (err) {
				return callback(err, null);
			}

			let imagesArray = user.images;
			let containsImageNumber = false;

			for (var i = 0; i < imagesArray.length; i++) {
				if (Object.keys(imagesArray[i])[0] == imageNumber) {
					imagesArray[i][imageNumber] = dataUrl;
					containsImageNumber = true;
				}
			}

			if (!containsImageNumber) {
				imagesArray.push(imageObject);
			}

			user.images = imagesArray;
			MongoConnection.db.collection('users').save(
				user,
				(err, data) => {
					if (err) {
						return callback(err, null);
					}
					return callback(null, data);
				}
			)
		}
	)
}

module.exports.getImages = function (userId, callback) {
	MongoConnection.db.collection('users').findOne(
		{ _id: userId },
		(err, profile) => {
			if (err) {
				return callback(err, null);
			} else {
				return callback(null, profile['images']);
			}
		}
	);
}

module.exports.getLocation = function (userId, callback) {
	MongoConnection.db.collection('users').findOne(
		{ _id: userId },
		(err, data) => {
			if (err) {
				return callback(err, null);
			}
			return callback(null, data['location']['coordinates']);
		}
	)
}

module.exports.updateLocation = function (userId, coords, callback) {
	let locationObject = {
		type: "Point",
		coordinates: coords
	}

	MongoConnection.db.collection('users').updateOne(
		{ _id: userId },
		{
			$set: {
				location: locationObject
			}
		},
		(err, msg) => {
			if (err) {
				return callback(err, null);
			}
			return callback(null, msg);
		}
	);
}

module.exports.verifyEmail = function (username, token, callback) {
	MongoConnection.db.collection('users').findOne(
		{ username: username },
		(err, user) => {
			if (err) {
				return callback(err, null);
			}

			if (user.token === token) {
				MongoConnection.db.collection('users').updateOne(
					user,
					{
						$set: {
							token: "",
							active: true
						}
					},
					(err, msg) => {
						if (err) {
							return callback(err, null);
						}
						return callback(null, { success: true, msg: "You have successfully verified your email. You may now authenticate." });
					}
				)
			} else {
				return callback(null, { success: false, msg: "You have provided an incorrect token" });
			}
		}
	)
}

module.exports.generatePasswordToken = function (username, token, callback) {
	MongoConnection.db.collection('users').findOneAndUpdate(
		{ username: username },
		{
			$set: {
				passwordToken: token
			}
		},
		(err, doc) => {
			if (err) {
				return callback(err, null);
			} else if (!doc.value) {
				return callback(null, { success: false, msg: "User not found." });
			} else {
				return callback(null, { success: true, msg: "Your password must be reset by entering the token.", token: token, email: doc.value.email });
			}
		}
	);
}

module.exports.updatePassword = function (username, token, password, callback) {
	MongoConnection.db.collection('users').findOne(
		{ username: username },
		(err, user) => {
			if (err) {
				return callback(err, null);
			} else {

				if (!user) {
					return callback(null, { success: false, msg: "User not found." });
				}

				if (user['passwordToken'] && user.passwordToken === token) {
					bcrypt.genSalt(10, (err, salt) => {
						if (err) {
							return callback(err, null);
						}
						bcrypt.hash(password, salt, (err, hash) => {
							if (err) {
								return callback(err, null);
							}
							password = hash;
							MongoConnection.db.collection('users').updateOne(
								user,
								{
									$set: {
										passwordToken: "",
										password: password
									}
								},
								(err, msg) => {
									if (err) {
										return callback(err, null);
									} else {
										return callback(null, { success: true, msg: "Your password has been successfully updated." });
									}
								}
							);
						});
					});
				} else {
					return callback(null, { success: false, msg: "Incorrect token." });
				}
			}
		}
	)
}

module.exports.findByLocation = function (userId, distance, callback) {

	MongoConnection.db.collection('users').findOne({ _id: userId }, (err, doc) => {
		
		let gender;
		let preference;

		if (doc.gender === "Male") {
			if (doc.sexual_preference === "Heterosexual") {
				gender = "Female";
				preference = [
						{sexual_preference: "Heterosexual"},
						{sexual_preference: ""},
						{sexual_preference: "Bisexual"}
					];
			} else if (doc.sexual_preference === "Homosexual") {
				gender = "Male";
				preference = [
						{sexual_preference: "Homosexual"},
						{sexual_preference: ""},
						{sexual_preference: "Bisexual"}
					];
			}
		} else if (doc.gender === "Female") {
			if (doc.sexual_preference === "Heterosexual") {
				gender = "Male";
				preference = [
						{sexual_preference: "Heterosexual"},
						{sexual_preference: ""},
						{sexual_preference: "Bisexual"}
					];
			} else if (doc.sexual_preference === "Homosexual") {
				gender = "Female";
				preference = [
						{sexual_preference: "Homosexual"},
						{sexual_preference: ""},
						{sexual_preference: "Bisexual"}
					];
			}
		} else {
			gender = "";
			preference = [{sexual_preference: ""}, {sexual_preference: "Bisexual"}];
		}

		if (err) {
			return callback(err, null);
		} else {
			MongoConnection.db.collection('users').find(
				{
					location: {
						$near: {
							$geometry: {
								type: "Point", coordinates: [doc.location.coordinates[0], doc.location.coordinates[1]]
							},
							$maxDistance: distance
						}
					},
					gender: gender,
					$or: preference
				},
				{
					fields: {
						email: 0,
						password: 0
					}
				}
			).toArray((err, result) => {
				if (err) {
					return callback(err, null);
				} else {
					for (let i = 0; i < result.length; i++) {
						if (doc.username === result[i].username) {
							result.splice(i, 1);
						}
					}
					return callback(null, result);
				}
			});
		}
	});
}