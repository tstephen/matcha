const config = require('./database');
const MongoClient = require('mongodb').MongoClient;

let db = null;

const MongoConnection = {
    connect() {
        return new Promise((resolve, reject) => {
            if (!db)
                MongoClient.connect(config.database, (err, client) => {
                    if (err) {
                        reject(err);
                    }
                    this.db = client.db('matcha');
                    resolve();
                    console.log('Connected to Mongo database.');
                });
        });
    },
};

module.exports = MongoConnection;
/* module.exports = mongoClient.connect(config.database); */
/* this.db.collection('users').find({}).toArray((err, docs) => {
    console.log(docs);
}); */