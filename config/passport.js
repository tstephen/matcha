const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('../models/userModel');
const config = require('../config/database');

module.exports = function(passport) {
	let opts = {};
	opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("jwt");
	opts.secretOrKey = config.secret;
	passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
		User.getUserById(jwt_payload._id, (err, user) => {
			if (err) {
				return done(err, false);
			}

            // TODO: Uncomment this when user can be made active
			if (user.active == false) {
				return done(null, false);
			}

            let signUser = {
                _id: user._id,
                fname: user.fname,
                lname: user.lname,
                email: user.email,
                username: user.username,
                active: user.active
            };

			if (user) {
				return done(null, signUser);
			} else {
				return done(null, false);
			}
		});
	}));
}