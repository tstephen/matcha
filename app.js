const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const config = require('./config/database');
const MongoConnection = require('./config/connection');

//Connect to MongoDb
MongoConnection.connect();

/*// On Connection
mongoose.connection.on('connected', () => {
	console.log('Connected to database ' + config.database);
});

// On Error
mongoose.connection.on('error', (err) => {
	console.log('Database error: ' + err);
});*/

const app = express();

const users = require('./routes/users');

// Cors middleware
app.use(cors());

// Set Static Folder
app.use(express.static(path.join(__dirname, 'public')));

// Body parser middleware
app.use(bodyParser.json({limit: '50MB'}));

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

const port = 3000;

app.use('/users', users);

// Index Route
app.get('/', (req, res) => {
    // TODO: remove this
    MongoConnection.db.collection('users').find({}).toArray((err, docs) => {
        res.send(docs);
    });
});

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/index.html'));
});

// Start server
app.listen(port, () => {
    console.log('Server started on port ' + port);
});