import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';

@Injectable({
	providedIn: 'root'
})

export class AuthService {
	authToken: any;
	user: any;
	
	constructor(private _jwtHelper: JwtHelperService, private _http: HttpClient, private _router: Router) { }

	registerUser(user): Observable<any> {
		let headers = new HttpHeaders();
		headers.append('Content-Type', 'application/json');
		return this._http.post('http://localhost:3000/users/register', user, { headers: headers }).pipe(
			map(this.extractData),
			catchError(this.handleError));
	}

	authenticateUser(user): Observable<any> {
		let headers = new HttpHeaders();
		headers.append('Content-Type', 'application/json');
		return this._http.post('http://localhost:3000/users/authenticate', user, { headers: headers }).pipe(
			map(this.extractData),
			catchError(this.handleError));;
	}

	private extractData(res: Response) {
        let body = res;
		return body || {};
	}

	private handleError(error: HttpErrorResponse) {
		if (error.error instanceof ErrorEvent) {
			// A client-side or network error occurred. Handle it accordingly.
			console.error('An error occurred:', error.error.message);
		} else {            
			// The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
			console.error(
				`Backend returned code ${error.status}, ` +
				`body was: ${error.error}`);
		}
		// return an observable with a user-facing error message
		return throwError('Something bad happened; please try again later.');
    };

	getProfile(): Observable<any> {
		this.loadToken();
		let headers = new HttpHeaders({
			'Content-Type': 'application/json',
			'Authorization': this.authToken
		});

		return this._http.get('http://localhost:3000/users/profile', { headers: headers }).pipe(
			map(this.extractData),
			catchError(this.handleError));
	}

	storeUserData(token, user) {
		localStorage.setItem('id_token', token);
		localStorage.setItem('user', JSON.stringify(user));
		this.authToken = token;
		this.user = user;
	}

	getUserId() {
		return JSON.parse(localStorage.getItem('user')).id;
	}

	loadToken() {
		const token = localStorage.getItem('id_token');
		this.authToken = token;
	}

	loggedIn() {
		return (!this._jwtHelper.isTokenExpired());
	}

	logout() {
		this.authToken = null;
		this.user = null;
		localStorage.clear();
	}

	updateProfile(toUpdate): Observable<any> {
		this.loadToken();
		let headers = new HttpHeaders({
			'Content-Type': 'application/json',
			'Authorization': this.authToken
		});

		return this._http.post('http://localhost:3000/users/updateProfile', toUpdate, { headers: headers }).pipe(
			map(this.extractData),
			catchError(this.handleError)
		);
	}

	getInterests() {
		this.loadToken();
		let headers = new HttpHeaders({
			'Content-Type': 'application/json',
			'Authorization': this.authToken
		});

		return this._http.get('http://localhost:3000/users/getUserInterests', { headers: headers }).pipe(
			map(this.extractData),
			catchError(this.handleError));
	}

	updateUserInterests(interests) {
		interests = JSON.stringify(interests);
		this.loadToken();
		let headers = new HttpHeaders({
			'Content-Type': 'application/json',
			'Authorization': this.authToken
		});

		return this._http.post('http://localhost:3000/users/updateUserInterests', interests,{ headers: headers }).pipe(
			map(this.extractData),
			catchError(this.handleError));
	}
	
	getGender() {
		this.loadToken();
		let headers = new HttpHeaders({
			'Content-Type': 'application/json',
			'Authorization': this.authToken
		});

		return this._http.get('http://localhost:3000/users/getUserGender', { headers: headers }).pipe(
			map(this.extractData),
			catchError(this.handleError));
	}

	getSexualPreference() {
		this.loadToken();
		let headers = new HttpHeaders({
			'Content-Type': 'application/json',
			'Authorization': this.authToken
		});

		return this._http.get('http://localhost:3000/users/getUserSexualPreference', { headers: headers }).pipe(
			map(this.extractData),
			catchError(this.handleError));
	}

	updateUserGender(gender: string | null) {
		this.loadToken();
		let headers = new HttpHeaders({
			'Content-Type': 'application/json',
			'Authorization': this.authToken
		});

		return this._http.post('http://localhost:3000/users/updateUserGender', {'Gender': gender}, { headers: headers }).pipe(
			map(this.extractData),
			catchError(this.handleError));
	}

	updateUserSexualPreference(preference: string | null) {
		this.loadToken();
		let headers = new HttpHeaders({
			'Content-Type': 'application/json',
			'Authorization': this.authToken
		});

		return this._http.post('http://localhost:3000/users/updateUserSexualPreference', {'Sexual_Preference': preference}, { headers: headers }).pipe(
			map(this.extractData),
			catchError(this.handleError));
	}

	getUserBio() {
		this.loadToken();
		let headers = new HttpHeaders({
			'Content-Type': 'application/json',
			'Authorization': this.authToken
		});

		return this._http.get('http://localhost:3000/users/getUserBiography', { headers: headers }).pipe(
			map(this.extractData),
			catchError(this.handleError));
	}	

	updateUserBio(biographyText: string) {
		this.loadToken();
		let headers = new HttpHeaders({
			'Content-Type': 'application/json',
			'Authorization': this.authToken
		});

		return this._http.post('http://localhost:3000/users/updateUserBiography', { biographyText: biographyText }, { headers: headers }).pipe(
			map(this.extractData),
			catchError(this.handleError));
	}	

	getUserDateOfBirth() {
		this.loadToken();
		let headers = new HttpHeaders({
			'Content-Type': 'application/json',
			'Authorization': this.authToken
		});

		return this._http.get('http://localhost:3000/users/getUserDateOfBirth', { headers: headers }).pipe(
			map(this.extractData),
			catchError(this.handleError));
	}

	updateUserDateOfBirth(dateOfBirth: Object) {
		this.loadToken();
		let headers = new HttpHeaders({
			'Content-Type': 'application/json',
			'Authorization': this.authToken
		});

		return this._http.post('http://localhost:3000/users/updateUserDateOfBirth', { birthDate: dateOfBirth }, { headers: headers }).pipe(
			map(this.extractData),
			catchError(this.handleError));
	}
	
	uploadImage(imageId, image) {
		this.loadToken();
		let headers = new HttpHeaders({
			'Content-Type': 'application/json',
			'Authorization': this.authToken
		});

		return this._http.post('http://localhost:3000/users/uploadImage', { imageNumber: imageId, dataURL: image }, { headers: headers }).pipe(
			map(this.extractData),
			catchError(this.handleError));
	}

	getImages() {
		this.loadToken();
		let headers = new HttpHeaders({
			'Content-Type': 'application/json',
			'Authorization': this.authToken
		});

		return this._http.get('http://localhost:3000/users/getImages', { headers: headers }).pipe(
			map(this.extractData),
			catchError(this.handleError));
	}

	getLocation() {
		this.loadToken();
		let headers = new HttpHeaders({
			'Content-Type': 'application/json',
			'Authorization': this.authToken
		});

		return this._http.get('http://localhost:3000/users/getLocation', { headers: headers }).pipe(
			map(this.extractData),
			catchError(this.handleError));
	}

	updateLocation(latitude, longitude) {
		let location = {
			latitude: latitude,
			longitude: longitude
		};

		this.loadToken();
		let headers = new HttpHeaders({
			'Content-Type': 'application/json',
			'Authorization': this.authToken
		});

		return this._http.post('http://localhost:3000/users/updateLocation', location, { headers: headers }).pipe(
			map(this.extractData),
			catchError(this.handleError));
	}

	verifyEmail(username, token) {
		let headers = new HttpHeaders({
			'Content-Type': 'application/json'
		});

		return this._http.post('http://localhost:3000/users/verifyEmail', { username: username, token: token }, { headers: headers }).pipe(
			map(this.extractData),
			catchError(this.handleError));
	}

	forgotPassword(username) {
		let headers = new HttpHeaders({
			'Content-Type': 'application/json'
		});

		return this._http.post('http://localhost:3000/users/forgotPassword', { username: username }, { headers: headers }).pipe(
			map(this.extractData),
			catchError(this.handleError));
	}

	resetPassword(username, token, password) {
		let headers = new HttpHeaders({
			'Content-Type': 'application/json'
		});

		return this._http.post('http://localhost:3000/users/newPassword', { username: username, token: token, password: password }, { headers: headers }).pipe(
			map(this.extractData),
			catchError(this.handleError));
	}

	getSuggestions() {
		this.loadToken();
		let headers = new HttpHeaders({
			'Content-Type': 'application/json',
			'Authorization': this.authToken
		});

		return this._http.post('http://localhost:3000/users/getSuggestionsByLocation', { distance: 25000 }, { headers: headers }).pipe(
			map(this.extractData),
			catchError(this.handleError));
	}
}

