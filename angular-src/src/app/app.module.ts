import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { JwtModule } from '@auth0/angular-jwt';

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { HomeComponent } from "./components/home/home.component";
import { LoginComponent } from "./components/login/login.component";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { ProfileComponent } from "./components/profile/profile.component";
import { RegisterComponent } from "./components/register/register.component";

import { EmailValidatorDirective } from './directives/email-validator.directive';
import { PasswordValidatorDirective } from './directives/password-validator.directive';

import { AuthService } from './services/auth.service';
import { AuthGuard } from './guards/auth.guard';
import { UpdateProfileComponent } from './components/update-profile/update-profile.component';
import { BiographyComponent } from './components/biography/biography.component';
import { BirthDateComponent } from './components/birth-date/birth-date.component';
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { ProfileImagesComponent } from './components/profile-images/profile-images.component';

import { AgmCoreModule } from '@agm/core';
import { MapComponent } from './components/map/map.component';
import { VerifyEmailComponent } from './components/verify-email/verify-email.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { UserProfileTagComponent } from './components/user-profile-tag/user-profile-tag.component';


export function tokenGetter() {
	return localStorage.getItem('id_token');
}

@NgModule({
	declarations: [
		AppComponent,
		DashboardComponent,
		HomeComponent,
		LoginComponent,
		NavbarComponent,
		ProfileComponent,
		RegisterComponent,
		EmailValidatorDirective,
		PasswordValidatorDirective,
		UpdateProfileComponent,
		BiographyComponent,
		BirthDateComponent,
		ProfileImagesComponent,
		MapComponent,
		VerifyEmailComponent,
		ForgotPasswordComponent,
		UserProfileTagComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		FormsModule,
		HttpClientModule,
		JwtModule.forRoot({
			config: {
				tokenGetter: tokenGetter
			}
		}),
		AgmCoreModule.forRoot({
			apiKey: 'AIzaSyDXJRvI77JFsVz4jqhB27dEYz3ICsR9eIs'
		}),
		BsDatepickerModule.forRoot()
	],
	providers: [AuthService, AuthGuard],
	bootstrap: [AppComponent]
})
export class AppModule { }

