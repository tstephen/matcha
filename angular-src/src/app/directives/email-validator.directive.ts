import {
    Validator,
    NG_VALIDATORS,
    FormControl
} from '@angular/forms';

import { Directive } from '@angular/core';

@Directive({
    selector: '[appEmailValidator]',
    providers: [{
        provide: NG_VALIDATORS,
        useExisting: EmailValidatorDirective,
        multi: true
    }]
})
export class EmailValidatorDirective implements Validator {

    constructor() {}

    validate(c: FormControl): { [key: string]: any } | null {
        let isValid = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(c.value);
        return (isValid == false ? { 'invalidEmail' : true} : null);
    }
}
