import {
    Validator,
    NG_VALIDATORS,
    FormControl
} from '@angular/forms';

import { Directive } from '@angular/core';

@Directive({
    selector: '[appPasswordValidator]',
    providers: [{
        provide: NG_VALIDATORS,
        useExisting: PasswordValidatorDirective,
        multi: true
    }]
})
export class PasswordValidatorDirective implements Validator {

    constructor() { }

    validate(c: FormControl): { [key: string]: any } | null {
        let isValid = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/.test(c.value);
        return isValid ? null : { 'invalidPassword': true };
    }
}
