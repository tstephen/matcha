import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-update-profile',
    templateUrl: './update-profile.component.html',
    styleUrls: ['./update-profile.component.css']
})

export class UpdateProfileComponent implements OnInit {

	user$: Observable<Object>;
	interests$: Observable<Object>;
	userInterests: Object;
	fname: string;
	lname: string;
	password: string;
	username: string;
	email: string;
	newData: Object;
	profileErrors: string | null;
	profileSuccess: string | null;
	interestKeys = Object.keys;
	interests: Object = {
		"Personal growth": false,
		"Health and exercise": false,
		"Food": false,
		"Family": false,
		"Dancing": false,
		"Politics": false,
		"Travel": false,
		"Theater": false,
		"Learning": false,
		"Photography": false,
		"Charitable": false,
		"Finding love": false,
		"Outdoor interests": false,
		"Art": false,
		"Friends": false,
		"Work / Career": false
	};
	interestsErrors: string | null;
	interestsSuccess: string | null;
	gender$: Observable<Object>;
	gender: string | null;
	genderErrors: string | null;
	genderSuccess: string | null;

	sexualPreference$: Observable<Object>;
	sexualPreference: string | null;
	sexualPreferenceErrors: string | null;
	sexualPreferenceSuccess: string | null;

    constructor(private _auth: AuthService, private _router: Router) {
    }
    
    ngOnInit() {
		this.user$ = this._auth.getProfile();
		this.interests$ = this._auth.getInterests();

		this.interests$.subscribe(interests => {
			for (let key in interests) {
				this.interests[key] = interests[key];
			}
		});

		this.gender$ = this._auth.getGender();
		this.gender$.subscribe(msg => {
			if (msg["success"]) {
				this.gender = msg["gender"];
			} else {
				this.gender = null;
			}
		});

		this.sexualPreference$ = this._auth.getSexualPreference();
		this.sexualPreference$.subscribe(msg => {
			if (msg["success"]) {
				this.sexualPreference = msg["sexual_preference"];
			} else {
				this.sexualPreference = "Bisexual";
			}
		});
	}

	onUpdateProfile() {
		this.newData = {
			fname: this.fname,
			lname: this.lname,
			password: this.password,
			username: this.username,
			email: this.email
		};

		this._auth.updateProfile(this.newData).subscribe((msg) => {			
			if (msg.success === false) {
				this.profileErrors = msg.msg;
				this.profileSuccess = null;
			} else if (msg._id) {
				this.profileSuccess = msg.msg;
				this.profileErrors = null;
			} else {
				this.profileErrors = null;
				this.profileSuccess = null;
			}

			this._auth.logout();
			this._router.navigate(['']);

			this.user$ = this._auth.getProfile();
			this.fname = undefined;
			this.lname = undefined;
			this.password = undefined;
			this.username = undefined;
			this.email = undefined;	
		});

		setTimeout(() => {
			this.profileErrors = null;
			this.profileSuccess = null;
		}, 3000);
	}

	onUpdateInterests() {
		this._auth.updateUserInterests(this.interests).subscribe(data => {
			if (data['success'] == 'false') {
				this.interestsErrors = "Something went wrong. Try again later";
				this.interestsSuccess = null;
			} else {
				this.interestsErrors = null;
				this.interestsSuccess = data['msg'];
			}
		});

		setTimeout(() => {
			this.interestsErrors = null;
			this.interestsSuccess = null;
		}, 3000);
	}

	onUpdateGender() {
		this._auth.updateUserGender(this.gender).subscribe(data => {
			if (data["success"]) {
				this.genderSuccess = data['msg'];
			} else {
				this.genderErrors = data['msg'];
			}
		});

		setTimeout(() => {
			this.genderErrors = null;
			this.genderSuccess = null;
		}, 3000);
	}

	onUpdateSexualPreference() {
		this._auth.updateUserSexualPreference(this.sexualPreference).subscribe(data => {
			if (data["success"]) {
				this.sexualPreferenceSuccess = data['msg'];
			} else {
				this.sexualPreferenceErrors = data['msg'];
			}
		});

		setTimeout(() => {
			this.sexualPreferenceSuccess = null;
			this.sexualPreferenceErrors = null;
		}, 3000);
	}
}
