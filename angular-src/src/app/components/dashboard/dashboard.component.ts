import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {

	suggestions: Array<Object> | null;
	defaultImage: string = "../../../assets/defaultProfile.jpg";
	
    constructor(private _auth: AuthService) {
    }

    ngOnInit() {
		this._auth.getSuggestions().subscribe(data => {
			if (data['success']) {
				if (data['suggestions'].length > 0) {
					this.suggestions = data['suggestions'];
				}
			} else {
				this.suggestions = null;
			}
		})
	}
}
