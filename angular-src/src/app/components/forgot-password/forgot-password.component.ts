import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
	selector: 'app-forgot-password',
	templateUrl: './forgot-password.component.html',
	styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

	username: string;
	success: string | null;
	errors: string | null;
	foundUser: boolean = false;
	token: string | null;
	password: string | null;
	confirmPassword: string | null;

	constructor(private _auth: AuthService) { }

	ngOnInit() {
	}

	onForgotPassword() {
		this._auth.forgotPassword(this.username).subscribe(data => {
			if (data['success']) {
				this.success = data['msg'];
				this.success += " Please check your email.";
				this.foundUser = true;
			} else {
				this.errors = data['msg'];
			}
		});

		setTimeout(() => {
			this.errors = null;
			this.success = null;
		}, 8000);
	}

	onResetPassword() {
		if (this.password !== this.confirmPassword) {
			this.errors = "Passwords do not match";
			return;
		}

		if (!this.token || !this.username) {
			this.errors = "Please complete all the information";
			return;
		}

		this._auth.resetPassword(this.username, this.token, this.password).subscribe( data => {
			if (data['success']) {
				this.success = data['msg'] + " You may now login.";
			} else {
				this.errors = data['msg'];
			}
		});
	}

}
