import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    fnameText: String;
    lnameText: String;
    emailText: String;
    usernameText: String;
    passwordText: String;
    submitted = false;
	success: string | null;
	errors: string | null;

    constructor(
        private _authService: AuthService,
        private _router: Router   
    ) { }

    ngOnInit() { }

    onRegisterSubmit() {
        const user = {
            fname: this.fnameText,
            lname: this.lnameText,
            email: this.emailText,
            username: this.usernameText,
            password: this.passwordText
        };

        this.submitted = true;
        // Register user
        this._authService.registerUser(user).subscribe((data) => {
			if (data.success) {
				this.success = data.msg;
				setTimeout(() => {
					this._router.navigate(['/login']);
				}, 2000);
             } else {
				this.errors = data.msg;
			 }
			 
			 setTimeout(() => {
				 this.errors = null;
				 this.success = null;
			 }, 3000);
        });
    }
    
    get diagnostic() { return JSON.stringify({ fname: this.fnameText, lname: this.lnameText, email: this.emailText, username: this.usernameText, password: this.passwordText}); }
}
