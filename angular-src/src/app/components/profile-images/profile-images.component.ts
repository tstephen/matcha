import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
	selector: 'app-profile-images',
	templateUrl: './profile-images.component.html',
	styleUrls: ['./profile-images.component.css']
})
export class ProfileImagesComponent implements OnInit {

	selectedFile: File;
	selectedImage = null;
	errors: string | null;
	success: string | null;
	images = new Array(5);

	constructor(private _auth: AuthService) { }

	ngOnInit() {

		this.images[0] = "../../../assets/defaultProfile.jpg";
		this.images[1] = "../../../assets/defaultProfile.jpg";
		this.images[2] = "../../../assets/defaultProfile.jpg";
		this.images[3] = "../../../assets/defaultProfile.jpg";
		this.images[4] = "../../../assets/defaultProfile.jpg";

		this._auth.getImages().subscribe(data => {
			if (data['success']) {
				for (let i = 0; i < data['images'].length; i++) {
					let index = parseInt(Object.keys(data['images'][i])[0]);
					this.images[index - 1] = data['images'][i][index];
				}
			}
		});
	}

	onFileSelected(event) {
		this.selectedFile = event.target.files[0];
	}

	onFileUpload() {
		if (this.selectedImage == null) {
			this.errors = "Please select an image to replace first.";
			return;
		}

		if (this.selectedFile == null) {
			this.errors = "Choose a picture to upload first.";
			return;
		}

		setTimeout(() => {
			this.errors = null;
			this.success = null;
		}, 3000);

		let reader = new FileReader();

		reader.onload = () => {
			this._auth.uploadImage(this.selectedImage.target.id, reader.result).subscribe(data => {
				if (data['success']) {
					this._auth.getImages().subscribe(data => {
						if (data['success']) {
							for (let i = 0; i < data['images'].length; i++) {
								let index = parseInt(Object.keys(data['images'][i])[0]);
								this.images[index - 1] = data['images'][i][index];
							}
						}
					});
				}
			});
		};

		reader.readAsDataURL(this.selectedFile);
	}

	selectImage(event: HTMLElement) {
		if (this.selectedImage) {
			this.selectedImage.target.style['border'] = '1px solid #dee2e6';
		}

		this.selectedImage = event;
		this.selectedImage.target.style['border'] = '2px solid #120afa';
	}
}
