import { Component, OnInit} from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

@Component({
	selector: 'app-birth-date',
	templateUrl: './birth-date.component.html',
	styleUrls: ['./birth-date.component.css']
})
export class BirthDateComponent implements OnInit {

	dateOfBirth: Date;
	success: string | null;
	errors: string | null;
	bsConfig: Partial<BsDatepickerConfig>;
	
	constructor(private _auth: AuthService) {
	}

	ngOnInit() {
		this._auth.getUserDateOfBirth().subscribe(data => {
			if (data['success']) {
				let day = data['birthdate']['day'];
				let month = data['birthdate']['month'];
				let year = data['birthdate']['year'];

				this.dateOfBirth = new Date();
				this.dateOfBirth.setDate(day); 
				this.dateOfBirth.setMonth(month - 1); 
				this.dateOfBirth.setFullYear(year); 
			}
		});

		this.bsConfig = Object.assign({}, { containerClass: 'theme-dark-blue' });
	}

	updateDateOfBirth() {
		let date = new Date(this.dateOfBirth);		
		
		let dateObject = {
			month: date.getMonth() + 1,
			day: date.getDate(),
			year: date.getFullYear()
		}

		this._auth.updateUserDateOfBirth(dateObject).subscribe(msg => {
			if (msg['success']) {
				this.success = msg['msg'];
				this.errors = null;
			} else {
				this.errors = msg['msg'];
				this.success = null;
			}

			setTimeout(() => {
				this.errors = null;
				this.success = null;
			}, 3000);
		});
	}
}
