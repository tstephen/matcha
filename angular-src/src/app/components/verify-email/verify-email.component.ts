import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
	selector: 'app-verify-email',
	templateUrl: './verify-email.component.html',
	styleUrls: ['./verify-email.component.css']
})
export class VerifyEmailComponent implements OnInit {

	success: string;
	errors: string;
	username: string;
	token: string;

	constructor(private _auth: AuthService) { }

	ngOnInit() {
	}

	onVerifyEmail() {
		this._auth.verifyEmail(this.username, this.token).subscribe( data => {
			if (data['success']) {
				this.success = data['msg'];
				this.errors = null;
			} else {
				this.errors = data['msg'];
				this.success = null;
			}

			setTimeout( () => {
				this.errors = null;
				this.success = null;
			}, 3000);
		});
	}
}
