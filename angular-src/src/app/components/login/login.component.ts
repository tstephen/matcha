import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	usernameText: String;
	passwordText: String;
	errors: String;

	constructor(private _auth: AuthService, private _router: Router) { }

	ngOnInit() {
	}

	onLoginSubmit() {
		const user = {
			username: this.usernameText,
			password: this.passwordText
		};
		this._auth.authenticateUser(user).subscribe(data => {
			if (data.success) {
				this._auth.storeUserData(data.token, data.user);
				this.errors = null;
				this._router.navigate(['/']);
			} else {
				this.errors = data.msg;
			}
		});
	}

}
