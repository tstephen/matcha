import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
	selector: 'app-map',
	templateUrl: './map.component.html',
	styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

	lat: number = -26.205308199999997;
	lng: number = 24;
	success: string | null;
	errors: string | null;

	constructor(private _auth: AuthService) { }

	ngOnInit() {
		this._auth.getLocation().subscribe(data => {
			if (data['success']) {
				this.lat = data['location'][1];
				this.lng = data['location'][0];
			}
		});

	}

	getLocation() {
		navigator.geolocation.getCurrentPosition((data) => {
			this.lat = data.coords.latitude;
			this.lng = data.coords.longitude;
		}, (err) => {
			//do nothing
		});
	}

	updateLocation() {
		this._auth.updateLocation(this.lat, this.lng).subscribe(data => {
			if (data['success']) {
				this.success = data['msg'];
			} else {
				this.errors = data['msg'];
			}

			setTimeout(() => {
				this.errors = null;
				this.success = null;
			}, 3000);
		});
	}

	onChoseLocation(event) {
		this.lat = event.coords.lat;
		this.lng = event.coords.lng;
	}

}
