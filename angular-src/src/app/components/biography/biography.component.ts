import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
	selector: 'app-biography',
	templateUrl: './biography.component.html',
	styleUrls: ['./biography.component.css']
})
export class BiographyComponent implements OnInit {

	biographyText: string;
	biographyErrors: string | null;
	biographySuccess: string | null;

	constructor(private _auth: AuthService) { }

	ngOnInit() {
		//Get existing biography via request
		this._auth.getUserBio().subscribe(data => {
			if (data['success']) {
				this.biographyText = data['bio'];
			}
		});
	}

	onUpdateBiography() {
		this._auth.updateUserBio(this.biographyText).subscribe(msg => {
			if (msg['success']) {
				this.biographySuccess = msg['msg'];
				this._auth.getUserBio().subscribe(data => {
					if (data['success']) {
						this.biographyText = data['bio'];
					}
				});
			} else {
				this.biographyErrors = msg['msg'];
			}
		});

		setTimeout(() => {
			this.biographySuccess = null;
			this.biographyErrors = null;
		}, 3000);
	}

}
