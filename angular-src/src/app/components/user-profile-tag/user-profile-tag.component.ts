import { Component, OnInit, Input } from '@angular/core';
import { ProfileImagesComponent } from '../profile-images/profile-images.component';

@Component({
	selector: 'app-user-profile-tag',
	templateUrl: './user-profile-tag.component.html',
	styleUrls: ['./user-profile-tag.component.css']
})
export class UserProfileTagComponent implements OnInit {

	@Input() profileImage;
	@Input() username: string;

	constructor() { }

	ngOnInit() {
	}

}
