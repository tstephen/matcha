import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserProfileTagComponent } from './user-profile-tag.component';

describe('UserProfileTagComponent', () => {
  let component: UserProfileTagComponent;
  let fixture: ComponentFixture<UserProfileTagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserProfileTagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProfileTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
