const express = require('express');
const router = express.Router();
const passport = require('passport');
const Users = require('../controllers/usersController');

// Register
router.post('/register', (req, res, next) => {

	const ipInfo = require("ipinfo");

	ipInfo((err, cLoc) => {
		
		let coords = [-26, 28];

		if (!err) {
			coords = cLoc['loc'].split(",");
			coords[0] = parseFloat(coords[0]);
			coords[1] = parseFloat(coords[1]);
		}

		let user = {
			fname: req.body.fname,
			lname: req.body.lname,
			email: req.body.email,
			username: req.body.username,
			password: req.body.password,
			active: false,
			latitude: coords[0],
			longitude: coords[1]
		}

		Users.registerUser(user, (err, msg) => {
			if (err) {
				res.json({ success: false, msg: "Something deep is happening at the moment. Try again later." });
			} else {
				res.json(msg);
			}
		});
	});
});

// Auth
router.post('/authenticate', (req, res, next) => {
	const username = req.body.username;
	const password = req.body.password;

	Users.loginUser(username, password, (err, msg) => {
		if (err) {
			res.json({ success: false, msg: "Something went wrong. Try again later." });
		} else {
			res.json(msg);
		}
	});
});

// Profile
router.get('/profile', passport.authenticate('jwt', { session: false }), (req, res, next) => {
	res.json({
		user: req.user,
	});
});

// Update profile
router.post('/updateProfile', passport.authenticate('jwt', { session: false }), (req, res, next) => {
	let newData = {
		fname: req.body.fname,
		lname: req.body.lname,
		email: req.body.email,
		username: req.body.username,
		password: req.body.password
	};

	Users.updateProfile(newData, req.user._id, (err, msg) => {
		if (err) {
			res.json({ success: false, msg: "Something went wrong. Try again later." });
		} else {
			res.json(msg);
		}
	});
});

router.get('/getUserInterests', passport.authenticate('jwt', { session: false }), (req, res) => {
	let defaultInterests = Users.getDefaultInterests();

	let interests = {};

	defaultInterests.forEach((element) => {
		interests[element] = false;
	});

	Users.getUserInterests(req.user._id, (err, msg) => {
		if (err) {
			res.json({ success: false, msg: "Something went wrong. Try again later." });
		} else {
			msg.forEach((element) => {
				interests[element] = true;
			});
			res.json(interests);
		}
	});
});

router.post('/updateUserInterests', passport.authenticate('jwt', { session: false }), (req, res) => {
	let defaultInterests = Users.getDefaultInterests();
	let interests = {};

	defaultInterests.forEach((element) => {
		if (req.body[element] === true) {
			interests[element] = true;
		}
	});

	// Pass to model to replace the interests field
	Users.updateUserInterests(req.user._id, interests, (msg) => {
		res.json(msg);
	});
});

router.post('/updateUserGender', passport.authenticate('jwt', { session: false }), (req, res) => {
	Users.updateUserGender(req.user._id, req.body['Gender'], (err, msg) => {
		if (err) {
			res.json({ success: false, msg: "Your gender was not updated. Try again later." });
		} else {
			res.json(msg);
		}
	});
});

router.get('/getUserGender', passport.authenticate('jwt', { session: false }), (req, res) => {
	Users.getUserGender(req.user._id, (err, gender) => {
		if (err) {
			res.json({ success: false, msg: "Your gender was not fetched. Please try again later." });
		} else {
			res.json(gender);
		}
	});
});

router.post('/updateUserSexualPreference', passport.authenticate('jwt', { session: false }), (req, res) => {
	Users.updateUserSexualPreference(req.user._id, req.body['Sexual_Preference'], (err, msg) => {
		if (err) {
			res.json({ success: false, msg: "Your sexual preference was not updated. Try again later." });
		} else {
			res.json(msg);
		}
	});
});

router.get('/getUserSexualPreference', passport.authenticate('jwt', { session: false }), (req, res) => {
	Users.getUserSexualPreference(req.user._id, (err, preference) => {
		if (err) {
			res.json({ success: false, msg: "Your sexual preference was not fetched. Please try again later." });
		} else {
			res.json(preference);
		}
	});
});

router.get('/getUserBiography', passport.authenticate('jwt', { session: false }), (req, res) => {
	Users.getUserBio(req.user._id, (msg) => {
		res.json(msg);
	});
});

router.post('/updateUserBiography', passport.authenticate('jwt', { session: false }), (req, res) => {
	Users.updateUserBio(req.user._id, req.body.biographyText, (msg) => {
		res.json(msg);
	});
});

router.get('/getUserDateOfBirth', passport.authenticate('jwt', { session: false }), (req, res) => {
	Users.getUserBirthDate(req.user._id, (msg) => {
		res.json(msg);
	})
});

router.post('/updateUserDateOfBirth', passport.authenticate('jwt', { session: false }), (req, res) => {
	Users.updateUserDateOfBirth(req.user._id, req.body.birthDate, (msg) => {
		res.json(msg);
	});
});

router.post('/uploadImage', passport.authenticate('jwt', { session: false }), (req, res) => {
	Users.uploadImage(req.user._id, req.body.imageNumber, req.body.dataURL, (msg) => {
		res.json(msg);
	});
});

router.get('/getImages', passport.authenticate('jwt', { session: false }), (req, res) => {
	Users.getImages(req.user._id, (msg) => {
		res.json(msg);
	});
});

router.get('/getLocation', passport.authenticate('jwt', { session: false }), (req, res) => {
	Users.getLocation(req.user._id, (msg) => {
		res.json(msg);
	});
});

router.post('/updateLocation', passport.authenticate('jwt', { session: false }), (req, res) => {
	Users.updateLocation(req.user._id, req.body.latitude, req.body.longitude, (msg) => {
		res.json(msg);
	});
});

router.post('/verifyEmail', (req, res) => {
	// User must provide username and token
	Users.verifyEmail(req.body.username, req.body.token, (msg) => {
		res.json(msg);
	});
});

router.post('/forgotPassword', (req, res) => {
	Users.forgotPassword(req.body.username, (msg) => {
		res.json(msg);
	})
});

router.post('/newPassword', (req, res) => {
	Users.newPassword(req.body.username, req.body.token, req.body.password, (msg) => {
		res.json(msg);
	})
});

//route to return all users within a certain location
router.post('/getSuggestionsByLocation', passport.authenticate('jwt', { session: false }), (req, res) => {
	Users.findByLocation(req.user._id, req.body.distance, (msg) => {
		res.json(msg);
	});
});

module.exports = router;